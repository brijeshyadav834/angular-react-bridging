import { Component } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { DATE } from "./modules/date-range/date-range.interface";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "reactDatePicker";
  dateForm: FormGroup;
  dateConfig: DATE;
  counter = 0;
  constructor(private fb: FormBuilder) {
    this.createForm();
  }
  createForm() {
    this.dateForm = this.fb.group({
      fromDate: ["", [Validators.required]],
      toDate: ["", [Validators.required]],
    });
    this.dateConfig = {
      fromDate: this.dateForm.get("fromDate") as FormControl,
      toDate: this.dateForm.get("toDate") as FormControl,
      label: "Select Date",
    };
  }
  getDate() {
    console.log(this.dateForm.value);
  }
  handleOnClick(event) {
    console.log(event);
  }
}
