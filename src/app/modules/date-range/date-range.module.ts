import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DateRangeComponent } from "./date-range.component";
import { AngularReactDatesModule } from "angular-react-dates";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [DateRangeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AngularReactDatesModule.forRoot(),
  ],
  exports: [DateRangeComponent],
})
export class DateRangeModule {}
