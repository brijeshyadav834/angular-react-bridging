import { FormControl } from "@angular/forms";

export interface DATE {
  fromDate: FormControl;
  toDate: FormControl;
  label?: string;
}
