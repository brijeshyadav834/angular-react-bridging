import { Component, Input, OnInit } from "@angular/core";
import { DATE } from "./date-range.interface";
import "react-dates/lib/css/_datepicker.css";
@Component({
  selector: "app-date-range",
  templateUrl: "./date-range.component.html",
  styleUrls: ["./date-range.component.scss"],
})
export class DateRangeComponent implements OnInit {
  selectedDateRange;
  dateRangePickerPros = {
    startDatePlaceholderText: "Start Date",
    endDatePlaceholderText: "End Date",
    showClearDates: true,
    // withPortal: true,
    // daySize: 30,
  };
  dateObject: DATE;
  @Input() set dateConfig(config) {
    this.dateObject = config;
  }
  constructor() {}

  ngOnInit(): void {}
  changeDateRangeAdv(event) {
    console.log(event);
  }
}
