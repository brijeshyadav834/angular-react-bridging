import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DateRangeWrapper } from "src/reactComponents/reactDateRangeWrapper";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent, DateRangeWrapper],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    // DateRangeModule,
    // AngularReactDatesModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
