import * as React from 'react';

import './dateRange.scss';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
// import 'react-dates/lib/css/_datepicker.css';
// require('../../node_modules/react-dates/lib/css/_datepicker.css');

export class ReactDateRange extends React.Component<any,any> {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props
    };
  }

  public getDate = date=>{
    this.setState({startDate:date.startDate,endDate:date.endDate});  
    this.props.dateChange(date);
  }
  
  render() {
    return (
      <DateRangePicker
  startDate={this.state.startDate} // momentPropTypes.momentObj or null,
  startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
  endDate={this.state.endDate} // momentPropTypes.momentObj or null,
  endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
  onDatesChange={({ startDate, endDate }) => this.getDate({ startDate, endDate })} // PropTypes.func.isRequired,
  focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
  onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
/>
    );
  }
}


