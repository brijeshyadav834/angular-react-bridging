import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    Output,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation
  } from '@angular/core';
import { ReactDateRange } from 'src/reactComponents/dateRangeReactComponent';
import * as React from 'react';

import * as ReactDOM from 'react-dom';
import * as moment from 'moment';
import 'react-dates/lib/css/_datepicker.css';

const containerElementName = 'myReactComponentContainer';
interface dateType{
  focusedInput: any,
    startDate: any,
    endDate: any,
    fullscreen: any,
    direction: any,
    dateFormat: any,
    small: any,
    block: any,
    orientation: any,
    numMonths: any,
    minimumNights: any
}
@Component({
    selector: 'app-react-date-range',
    template: `<span #${containerElementName}></span>`,
    styleUrls: ['./dateRange.scss'],
    encapsulation: ViewEncapsulation.None,
  })
  export class DateRangeWrapper implements OnChanges, OnDestroy, AfterViewInit {
    @ViewChild(containerElementName, {static: false}) containerRef: ElementRef;

    @Input() public counter = 10;
    @Output() public componentClick = new EventEmitter<any>();
    state;

    constructor() {
      this.state = {
        startDate: moment(),
        endDate: moment().add(7, 'days'),
        fullscreen: false,
        direction: 'left',
        dateFormat: 'MM/DD/YYYY',
        small: false,
        block: false,
        orientation: 'horizontal',
        numMonths: 2,
        minimumNights: 7,
      };
    }

   

   
    ngOnChanges(changes: SimpleChanges): void {
      // this.render();
    }
    public handleDateChange = (date)=>{
      console.log(date);
      // this.setState({startDate:date.startDate,endDate:date.enddate});
      this.state.startDate = date.startDate;
      this.state.endDate = date.endDate;
      
    }

    ngAfterViewInit() {
      this.render();
    }

    ngOnDestroy() {
      ReactDOM.unmountComponentAtNode(this.containerRef.nativeElement);
    }

    private render() {

      ReactDOM.render(<div >
        <ReactDateRange dateChange={this.handleDateChange} {...this.state}/>
      </div>, this.containerRef.nativeElement);
    }
  }
